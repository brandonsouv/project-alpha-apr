from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from projects.models import Project
from tasks.models import Task
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    num_list = projects.count()
    context = {"list_projects": projects, "num_list": num_list}
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project_to_show = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project_to_show.id)
    num_task = tasks.count()
    context = {
        "show_project": project_to_show,
        "num_task": num_task,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)


def redirect_to_list_projects(request):
    return redirect("list_projects")


# Create your views here.
