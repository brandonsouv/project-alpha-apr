from django.db import models
from projects.models import Project
from django.contrib.auth.models import User
from django.utils import timezone


class Task(models.Model):
    name = models.CharField(max_length=200, default="")
    start_date = models.DateTimeField(default=timezone.now())
    due_date = models.DateTimeField(default=timezone.now())
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.CASCADE, null=True
    )

    def _str_(self):
        return self.name


# Create your models here.
