from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    show_task = Task.objects.filter(assignee=request.user)
    num_task = show_task.count()
    context = {
        "show_my_tasks": show_task,
        "num_task": num_task,
    }
    return render(request, "tasks/list.html", context)


# Create your views here.
